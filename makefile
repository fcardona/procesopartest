# tests

CXX = g++
SRCDIR = src
BUILDDIR = build
LIBDIR = lib
SRCEXT = cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
CXXFLAGS = -Wall -Wextra -O2 -std=c++11 -g # -Wall -O2
LDFLAGS = -L $(LIBRARYDIR) -Wl,-rpath $(LIBRARYDIR)
LIB := -pthread -l$(LIBRARYNAME)
INC := -I $(LIBRARYINCLUDE) -I include
TARGET := bin/tests

.PHONY: all

all: ${TARGET}

$(TARGET): $(OBJECTS)
	@echo " Liking..."
	@echo " $(CXX) $^ -o $(TARGET) $(LDFLAGS) $(LIB)"; $(CXX) $^ -o $(TARGET) $(LDFLAGS) $(LIB)

$(BUILDDIR)/%.o : $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo " $(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<"; $(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<

clean:
	@echo " Cleaning ..."
	@echo " $(RM) -r $(BUILDDIR)"; $(RM) -r $(BUILDDIR)
	@echo " $(RM) -f $(TARGET)"; $(RM) -r $(TARGET)

.PHONY: clean

