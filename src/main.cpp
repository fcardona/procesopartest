#include <test.h>
#include <cstdlib>
#include <iostream>
#include <pthread.h>

static void usage(const char* prgname);

int
main(int argc, char *argv[]) {

  if (argc == 1)
    usage(argv[0]);

  
  // std::cout << "Argc: " << argc << std::endl  
  switch (argc) {
  case 2:
    test1(argv[1]);
    break;
  default:
    {
      const int nHilos = argc - 1;
      pthread_t **hilos = new pthread_t*[nHilos];
      for (int i = 1; i < argc; ++i) {
	hilos[i-1] = testm(argv[i]);
      }
      for (int i = 0; i < nHilos; ++i) {
	if (hilos[i]) {
	  void *retVal;
	  pthread_join(*hilos[i], &retVal);
	}
      }
    }
    break;
  }
  
  return EXIT_SUCCESS;
}

static void usage(const char* prgname) {
  std::cerr << "Usage: " << prgname
	    << " <filename> ..." << std::endl;
  exit(EXIT_SUCCESS);
}
