#include <ProcesoPar.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <test.h>
#include <pthread.h>
#include <errno.h>

typedef struct InfoHilo {
  ProcesoPar_t *pp;
  const char *filename;
} InfoHilo_t;

static int f(const void *buffer, unsigned int size);
static void* hiloEnvio(void *);

pthread_t* testm(const char* filename) {
  const char * const argv[] = { "cat", "-n", nullptr };
  const char * const env[] = { nullptr };
  
  ProcesoPar_t *pp = lanzarProcesoPar("/usr/bin/cat",
				      (char * const*) argv,
				      (char * const*) env);

  if (!pp) {
    std::cerr << "Error lanzado procesos par: "
	      << errno
	      << " "
	      << strerror(errno)
	      << std::endl;
    return nullptr;
  }

  establecerFuncionDeEscucha(pp, f);

  pthread_t*  hilo = new pthread_t;
  InfoHilo_t *ih = new InfoHilo();
  ih->pp = pp;
  ih->filename = filename;
  
  pthread_create(hilo, nullptr, hiloEnvio, ih);

  return hilo;
}

static int f(const void *buffer, unsigned int size) {
  char *pBuffer = new char[size + 1];

  strncpy(pBuffer, (char *) buffer, size);
  
  pBuffer[size] = '\0';
  
  std::cout << pBuffer << std::endl;
  
  delete pBuffer;
  
  return 0;
}

void*
hiloEnvio(void *arg) {
  struct InfoHilo *ih = (struct InfoHilo *) arg;

  std::cout << "filename: " << ih->filename << std::endl;

  std::ifstream in(ih->filename, std::ios_base::in);

  if (!in) std::cerr << "Error: " << ih->filename << std::endl;
  
  while (in) {
    std::string str;

    getline(in, str);
    str += '\n';
    enviarMensajeProcesoPar(ih->pp, str.c_str(), str.size());
  }

  int *retValue = new int; 
  
  *retValue = destruirProcesoPar(ih->pp);

  return retValue;
}
