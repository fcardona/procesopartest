#include <ProcesoPar.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

static int f(const void *buffer, unsigned int size) {
  char *pBuffer = new char[size + 1];

  strncpy(pBuffer, (char *) buffer, size);
  
  pBuffer[size] = '\0';
  
  std::cout << pBuffer << std::endl;
  
  delete pBuffer;
  
  return 0;
}

void
test1(const char *filename) {
  const char * const argv[] = { "cat", "-n" , nullptr };
  const char * const env[] = { nullptr };
  
  ProcesoPar_t *pp = lanzarProcesoPar("/usr/bin/cat",
				      (char * const*) argv,
				      (char * const*) env);
  
  establecerFuncionDeEscucha(pp, f);
  
  std::ifstream in(filename, std::ios_base::in);

  if (!in) std::cerr << "Error: " << filename << std::endl;
  
  while (in) {
    std::string str;

    getline(in, str);
    str += '\n';
    enviarMensajeProcesoPar(pp, str.c_str(), str.size());
  }
  
  destruirProcesoPar(pp);
}
